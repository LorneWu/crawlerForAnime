# -*- coding: utf-8 -*-
import os
import json
import threading
import requests
#from urllib.parse import urlparse
from bs4 import BeautifulSoup as bs
import time
import proxy
import csv
import re
import configparser
config = configparser.ConfigParser()
config.read('setting.ini')

if os.path.exists(config.get('file','logfile')):
    os.rename(config.get('file','logfile'),config.get('file','logfile')+time.strftime("%H %M %S"))

def writeJson(data):
    with open('data.txt', 'w') as outfile:
        json.dump(data, outfile)
def write_log(status,message):
    log_name = config.get('file', 'logfile')
    if not os.path.exists(log_name):
        with open(log_name,'w')as t:pass
    with open(log_name,'a',encoding ='utf-8') as f:
        f.write(status+"   \t:\t"+message+"\n")
        print('[%s]%s'%(time.strftime("%H:%M:%S"),status)+"   \t:\t"+message)
'''
def write_decorator(func):
    def wrapper(animax,*args,**kwargs):
        animax.writeloglock.acquire()
        animax.write_log()
        animax.writeloglock.release()
        res = func(*args,**kwargs)
        return res
    return wrapper
'''
class anime(threading.Thread):
    def __init__(self,url,id,writeloglock,proxylock,semaphoreRun,semaphoreGate):
        threading.Thread.__init__(self)
        self.writeloglock = writeloglock
        self.proxylock = proxylock
        self.semaphoreRun = semaphoreRun
        self.semaphoreGate = semaphoreGate
        self.id=id
        self.aid = url.split('/')[4]        
        self.title = url.split('/')[5]
        self.ip = ''
        self.write_log('M','get semaphoreGate')
        self.url = url
        self.url_main = url
        self.url_episode = url+'/episode'
        self.url_reviews = url+'/reviews'
        self.url_userrecs = url+'/userrecs'
        self.url_stats = url+'/stats'
        self.url_characters = url+'/characters'
        self.folder = os.path.join(config.get('downloads','downloads_folder'),self.aid+" "+self.title)
        if not os.path.exists(self.folder):os.makedirs(self.folder)
        self.write_log('M','get init done')
    def run(self):
        self.write_log('M','wait semaphoreRun')
        self.semaphoreRun.acquire()
        self.write_log('M','get semaphoreRun')
        self.write_log('M','wait proxylock')
        self.proxylock.acquire()    
        self.ip = proxy.getip()
        self.proxylock.release()
        self.write_log('M','release proxylock')        
        self.write_log('M','get first ip ')        

        self.main_csv = {}
        self.main_csv_detail={'Japanese':'','English':'','Synonyms':'','Genres':'','Type':'','Source':'','Status':'','Aired':'','Episodes':'','Duration':'','Premiered':'','Broadcast':'','Licensors':'','Producers':'','Studios':'','Score':'','Ranked':'','Rating':'','Favorites':'','Popularity':'','Members':'','relateAnime':'','opening':'','ending':''}
        self.main_csv_stats={'Watching':'','Completed':'','On-Hold':'','Dropped':'','Plan to Watch':'','Total':''}
        self.main_csv_characters={'Main':[],'Supporting':[],'Staff':[],'Staff_Detail':[]}
        #self.description = ''
        #self.relateAnime = {}
        #self.episodes = {}
        self.write_log('M',"start getting html.")
        while True:
            try:
                self.write_log('T','try '+self.url_main)
                sp_main = self.download_html(self.url_main,self.ip)
                self.write_log('S','get '+self.url_main)   
                self.write_log('T','try parse '+self.url_main)                
                self.getDetails(sp_main)
                self.write_log('S','get parse '+self.url_main) 
                self.write_log('T','try '+self.url_episode)                
                sp_episode = self.download_html(self.url_episode,self.ip)
                self.write_log('S','get '+self.url_episode)   
                self.write_log('T','try parse '+self.url_episode) 
                self.getEpisodes(sp_episode)
                self.write_log('S','get parse '+self.url_episode) 
                self.write_log('T','try '+self.url_stats)    
                sp_stats = self.download_html(self.url_stats,self.ip)
                self.write_log('S','get '+self.url_stats)   
                self.write_log('T','try parse '+self.url_stats) 
                self.getStats(sp_stats)
                self.write_log('S','get parse '+self.url_stats) 
                self.write_log('T','try '+self.url_reviews)    
                sp_reviews = self.download_html(self.url_reviews,self.ip)
                self.write_log('S','get '+self.url_reviews)   
                self.write_log('T','try parse '+self.url_reviews) 
                self.getReviews(sp_reviews)
                self.write_log('S','get parse '+self.url_reviews) 
                self.write_log('T','try '+self.url_characters)    
                sp_characters = self.download_html(self.url_characters,self.ip)
                self.write_log('S','get '+self.url_characters)   
                self.write_log('T','try parse '+self.url_characters) 
                self.getCharacters(sp_characters)
                self.write_log('S','get parse '+self.url_characters) 
                   
                if sp_main.find('title') == None or sp_episode.find('title') == None or sp_stats.find('title') == None or sp_reviews.find('title') == None or sp_characters.find('title') == None:raise Exception('error_html')
            except Exception as e:
                self.write_log('E',str(e))
                self.write_log('M','wait proxylock')
                self.proxylock.acquire()
                proxy.badip(self.ip)
                self.ip = proxy.getip()
                self.write_log('M'," get new ip")
                self.proxylock.release()
                self.write_log('M','release proxylock')
            else:
                self.proxylock.acquire()
                proxy.goodip(self.ip)
                self.proxylock.release()
                self.write_log('M',"finish getting html.")
                break
        #print(sp_main)
        #print(sp_episode)
        time.sleep(3)
        self.writeloglock.acquire()
        self.write_csv()
        self.writeloglock.release()
        self.semaphoreRun.release()
        self.write_log('M','release for semaphoreRun')
        self.semaphoreGate.release()
        self.write_log('M','release for semaphoreGate')
    def write_log(self,status,message):
        self.writeloglock.acquire()
        write_log('%s'%(status),'(id:%5s):%5s[%-21s]\t%s'%(self.id,self.aid,self.ip,message))

        self.writeloglock.release()
        #self.getEpisodes()
    def write_csv(self):
        self.main_csv.update(self.main_csv_detail)
        self.main_csv.update(self.main_csv_stats)
        for i in self.main_csv_characters:
            self.main_csv[i] = '||'.join(self.main_csv_characters[i]) 
        fieldnames = ['Japanese','English','Synonyms','Genres','Type','Source',
                      'Status','Aired','Episodes','Duration','Premiered','Broadcast',
                      'Licensors','Producers','Studios',
                      'Score','Ranked','Rating','Popularity','Favorites','Members',
                      'relateAnime','opening','ending',
                      'Watching','Completed','On-Hold','Dropped','Plan to Watch','Total',
                      'Main','Supporting','Staff','Staff_Detail']
        if not os.path.exists(config.get('file','output')):
            with open(config.get('file','output'), 'w', newline='') as f:
                writer = csv.DictWriter(f, fieldnames=fieldnames)
                writer.writeheader()
        with open(config.get('file','output'), 'a', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writerow(self.main_csv)

    def getDetails(self,sp):
        txt_description = ""
        #######################################################################
        tp = sp.find('div',{'class':'js-scrollfix-bottom'})
        for i in tp.find_all('div'):
            if i.find('span',{'class','dark_text'})!=None:
                if i.find('small')==None :
                    k = i.find('span',{'class':"dark_text"}).text
                    v = i.text.strip().replace(k,'').replace('\n','').replace('  ','')
                    for i in range(len(v)):
                        if v[i] != ' ':break
                    v = v[i:]
                    key = k.replace('"',' ').replace(':','')
                    value = v.replace('"',' ')
                    if key in self.main_csv_detail:
                        self.main_csv_detail[key] = value
                else:
                    if i.find_all('span')!=None:
                        if len(i.find_all('span'))==1:
                            key = i.find('span').text.replace('"',' ').replace(':','')
                            value = str(i)[str(i).find('</span>')+len('</span>'):str(i).find('<sup>')].strip().replace('"',' ')
                            if key in self.main_csv_detail:
                                self.main_csv_detail[key]=value
                        else:
                            s = str(i.text.strip().replace('\n',''))
                            t = str(s[:s.find(")")+1])
                            key = t[:t.find(':')].replace('"',' ').replace(':','')
                            value = t[t.find(':')+1:].replace('"',' ')
                            if key in self.main_csv_detail:
                                self.main_csv_detail[key] = value
        if sp.find('table',{"class":"anime_detail_related_anime"})!=None:
            t = sp.find('table',{"class":"anime_detail_related_anime"})
            tmp = ''
            k = ''
            if t.find_all('td')!=None:
                for i in t.find_all('td'):
                    if k == '' :k = i.text
                    else:
                        tmp+=k.replace('"',' ')+':'+i.text.replace('"',' ')+'\n'
                        k=''
                self.main_csv_detail['relateAnime'] = tmp
        if sp.find('div',{'class':"theme-songs js-theme-songs opnening"})!=None:
            tp = sp.find('div',{'class':"theme-songs js-theme-songs opnening"})
            if tp.find('span',{'class':'theme-song'})!=None:
                #print(tp.find_all('span',{'class':'theme-song'}))
                for i in tp.find_all('span',{'class':'theme-song'}):
                    self.main_csv_detail['opening']+=i.text+'\n'
        if sp.find('div',{'class':"theme-songs js-theme-songs ending"})!=None:
            tp = sp.find('div',{'class':"theme-songs js-theme-songs ending"})
            if tp.find('span',{'class':'theme-song'})!=None:
                for i in tp.find_all('span',{'class':'theme-song'}):
                    self.main_csv_detail['ending']+=i.text+'\n'

        if sp.find('span',{'itemprop':"description"})!=None:
            txt_description=sp.find('span',{'itemprop':"description"}).text.replace('"',' ')       
        with open(os.path.join(self.folder,config.get('file','description')),'w')as f:f.write(txt_description)
    def getEpisodes(self,sp):
        txt_episodes = [['vol','Episode Title','Episode Title Japan','Aired']]
        ############################################
        tp = sp.find('table',{'class':'mt8 episode_list js-watch-episode-list ascend'})
        if tp != None:
            if tp.find_all('tr',{'class':'episode-list-data'})!=None:
                for i in tp.find_all('tr',{'class':'episode-list-data'}):
                    txt_episodes.append([i.find('td',{'class':'episode-number nowrap'}).text,i.find('td',{'class':'episode-title'}).find('a').text,i.find('td',{'class':'episode-title'}).find('span',{'class':"di-ib"}).text,i.find('td',{'class':"episode-aired nowrap"}).text])
        with open(os.path.join(self.folder,config.get('file','episodes')),"w") as f:
            w = csv.writer(f)
            w.writerows(txt_episodes)
    def getStats(self,sp):
        txt_vote=[['star','percent','vots']]
        txt_stats=[['status','count']]

        tp = sp.find('div',{'class':"js-scrollfix-bottom-rel"})
        up = tp.table.extract()
        tp.table.decompose()
        if tp.find('div',{'class','spaceit_pad'})!=None:
            for i in tp.find_all('div',{'class','spaceit_pad'}):
                tmp = i.text.split(': ')
                self.main_csv_stats[tmp[0]]=tmp[1]
                txt_stats.append([tmp[0],tmp[1]])
        if up.find_all('tr')!=None:
            for i in up.find_all('tr'):
                if len(i.find_all('td'))==2:
                    key = i.find_all('td')[0].text
                    value = i.find_all('td')[1].text.replace('\xa0','')
                    txt_vote.append([key,value[:value.find(' ')],value[value.find(' ')+1:].replace('(','').replace(')','')])
        with open(os.path.join(self.folder,config.get('file','vote')),"w") as f:
            w = csv.writer(f)
            w.writerows(txt_vote)
        with open(os.path.join(self.folder,config.get('file','stats')),"w") as f:
            w = csv.writer(f)
            w.writerows(txt_stats)

    def getReviews(self,sp):
        txt_review = []
        tp = sp.find('div',{'class':"js-scrollfix-bottom-rel"})
        if tp!=None:
            for i in tp.find_all('div',{'class':'spaceit textReadability word-break pt8 mt8'}):
                txt_review.append(i.text.strip())
        with open(os.path.join(self.folder,config.get('file','reviews')),"w") as f:
            for i in txt_review:
                f.write(i)
                f.write('\n')
    def getCharacters(self,sp):
        csv_role = []
        csv_staff = []
        
        
        tp = sp.find('div',{'class':"js-scrollfix-bottom-rel"})
        if tp!=None:
            if tp.find_all('table')!=None:
                for i in tp.find_all('table'):
                    if i.find_all('td')!=None :
                        if len(i.find_all('td')) >= 2:
                            title = (i.find('small').text if i.find('small')!=None else '')
                            if title == 'Main':
                                name = (i.find_all('td')[1].find('a').text if i.find_all('td')[1].find('a')!=None else '')
                                for j in i.find_all('td')[2].find('table').find_all('tr'):
                                    actorname = (j.find('a').text if j.find('a')!=None else '')
                                    country = (j.find('small').text if j.find('small')!=None else '')
                                    csv_role.append([title,name,country,actorname])
                                    self.main_csv_characters['Main'].append(actorname+'('+country+')')
                            elif title == 'Supporting':
                                name = (i.find_all('td')[1].find('a').text if i.find_all('td')[1].find('a')!=None else '')
                                for j in i.find_all('td')[2].find('table').find_all('tr'):
                                    actorname = (j.find('a').text if j.find('a')!=None else '')
                                    country = (j.find('small').text if j.find('small')!=None else '')
                                    csv_role.append([title,name,country,actorname])
                                    self.main_csv_characters['Supporting'].append(actorname+'('+country+')')              
                            else:
                                name = (i.find_all('td')[1].find('a').text if i.find_all('td')[1].find('a')!=None else '')
                                if name!='\n\n':
                                    csv_staff.append([title,name])
                                    self.main_csv_characters['Staff'].append(name)
                                    self.main_csv_characters['Staff_Detail'].append(name+'('+title+')')
        with open(os.path.join(self.folder,config.get('file','character_role')),"w") as f:
            w = csv.writer(f)
            w.writerows(csv_role)            
        with open(os.path.join(self.folder,config.get('file','character_staff')),"w") as f:
            w = csv.writer(f)
            w.writerows(csv_staff)   
    def download_html(self,url,ip):
        import headers
        html_timeout = config.get('downloads', 'html_timeout')
        try:
            self.write_log("T",ip)
            proxies = {"http":'http://'+ip,"https":'https://'+ip}
            html = requests.get(url,proxies = proxies,headers = headers.getprofile() ,timeout = int(html_timeout))
            #html = requests.get(url,headers = headers.getprofile() ,timeout = int(html_timeout))

            #html = requests.get(urlf,headers = headers ,timeout = html_timeout)
            if html == None:raise Exception('HtmlNone')
            #print(html.text)
            sp = bs(html.text,'html.parser')
            #if not check_sp(sp,ip):raise Exception('CheckSpFail')
        except Exception as e:
            self.write_log("F",str(e))
            if e.args[0] == 'NotAvailable':
                return 'NotAvailable'
            time.sleep(int(config.get('downloads', 'fail_celeb')))
            return 'error_html'
        else:
            
            self.write_log("S",'get html')
            with open(os.path.join(self.folder,url.split('/')[-1]+'.html'),'w') as f:
                f.write(html.text)
            #sleep_awhile()
            time.sleep(int(config.get('downloads', 'finish_celeb')))            
            #break
            return sp
        
        
#############################################################################################################
def trace(url):
    def download_html(url,ip):
        import headers
        html_timeout = config.get('downloads', 'html_timeout')
        try:
            write_log_tmp("T",ip)
            proxies = {"http":'http://'+ip,"https":'https://'+ip}
            html = requests.get(url,proxies = proxies,headers = headers.getprofile() ,timeout = int(html_timeout))
            if html == None:raise Exception('HtmlNone')
            sp = bs(html.text,'html.parser')
        except Exception as e:
            write_log_tmp("F",str(e))
            if e.args[0] == 'NotAvailable':
                return 'NotAvailable'
            time.sleep(int(config.get('downloads', 'fail_celeb')))
            return 'error_html'
        else:
            write_log_tmp("S",'get html')
            time.sleep(int(config.get('downloads', 'finish_celeb')))            
            return sp
    def write_log_tmp(status,message):
        global writeloglock
        writeloglock.acquire()
        log_name = config.get('file', 'logfile')
        if not os.path.exists(log_name):
            with open(log_name,'w')as t:pass
        with open(log_name,'a',encoding ='utf-8') as f:
            f.write(status+"   \t:\t"+message+"\n")
            print('[%s]%s'%(time.strftime("%H:%M:%S"),status)+"   \t:\t"+message)
        writeloglock.release()
    #############
    global writeloglock
    #writeloglock = threading.Lock()
    global proxylock
    #proxylock = threading.Lock()
    global semaphoreRun 
    #semaphoreRun = threading.Semaphore(int(config.get('thread','runtime_threads')))
    global semaphoreGate
    #semaphoreGate = threading.Semaphore(int(config.get('thread','threadspool_limit')))
    #############
    global index
    global pages
    domain_name = url.split('?')[0]
    index = [url]
    index_tag = 0
    pages = []
    #pages_tag = 0
    ip = proxy.getip()
    while True:
        try:
            sp = download_html(index[index_tag],ip)
            if sp.find('a') != None:
                for i in sp.find_all('a'):
                    if i.get('href')!=None:
                        link = i.get('href')
                        pattern = re.compile(r'https://myanimelist.net/anime/\d+/[a-z\d]+')
                        if link.startswith('?') and domain_name+link not in index:
                            index.append(domain_name+link)
                        elif pattern.match(link) and link not in pages:
                            pages.append(link)
        except Exception as e:
            write_log_tmp('E',str(e))
            write_log_tmp('M','wait proxylock')
            proxylock.acquire()
            proxy.badip(ip)
            ip = proxy.getip()
            write_log_tmp('M'," get new ip")
            proxylock.release()
            write_log_tmp('M','release proxylock')
        else:
            proxylock.acquire()
            proxy.goodip(ip)
            proxylock.release()
            write_log_tmp('M',"finish getting html.")
            break
        
        index_tag+=1
        if index_tag >= len(index):break
        if index_tag>=3:break     
    threadspool = []           
    for id in range(len(pages)):
        write_log_tmp('M',"id:%s \t:wait semaphoreGate"%str(id))
        semaphoreGate.acquire()
        thr = anime(pages[id],id,writeloglock,proxylock,semaphoreRun,semaphoreGate)
        write_log_tmp('M',"id:%s \tget semaphoreGate"%(thr.id))
        thr.start()
        threadspool.append(thr)
    for j in threadspool:
        j.join()
        write_log_tmp('M',j.id+" \thave been join")

global writeloglock
writeloglock = threading.Lock()
global proxylock
proxylock = threading.Lock()
global semaphoreRun 
semaphoreRun = threading.Semaphore(int(config.get('thread','runtime_threads')))
global semaphoreGate
semaphoreGate = threading.Semaphore(int(config.get('thread','threadspool_limit')))
url = 'https://myanimelist.net/anime/28977/Gintama%C2%B0'
#a = anime(url,1,writeloglock,proxylock,semaphoreRun,semaphoreGate)
#a.start()
#a.join()