#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 14:50:43 2017

@author: lorne
"""
import os
import configparser
import os
import time
config = configparser.ConfigParser()
config.read('setting.ini')
global available_proxy
global useing_proxy
available_proxy = []
useing_proxy = []
proxy_history={}
def proxy_init():
    os.system('proxybroker find --types HTTP HTTPS --lvl High --strict -l '+config.get('proxy','proxy_count')+' > '+config.get('file', 'proxyfile'))
    proxy_getlist()
def proxy_getlist():
    global available_proxy
    with open(config.get('file', 'proxyfile'),'r') as f:
        for i in f.readlines():
            available_proxy.append(i[i.rfind(' ')+1:i.rfind('>')])
            if i[i.rfind(' ')+1:i.rfind('>')] in proxy_history:proxy_history[i[i.rfind(' ')+1:i.rfind('>')]][0]+=1 
            else: proxy_history[i[i.rfind(' ')+1:i.rfind('>')]] = [0,0]

def getip():
    global available_proxy
    global useing_proxy
    global proxy_history
    while True:
        global init_time
        if time.clock() - init_time > int(config.get('proxy','proxy_fresh_time'))/100:
            proxy_init()
        if len(available_proxy)>int(config.get('proxy','proxy_low_limit')):
            tmp = available_proxy[0]
            if (proxy_history[tmp][0] + proxy_history[tmp][1])  < int(config.get('proxy','bad_proxy_define')):
                badip(tmp)
                available_proxy.remove(tmp)
                continue
            else:
                useing_proxy.append(tmp)
                available_proxy.remove(tmp)
                return tmp
        else:
            proxy_init()
            return getip()
def badip(ip):
    global init_time
    if time.clock() - init_time > int(config.get('proxy','proxy_fresh_time'))/100:
        proxy_init()
    global available_proxy
    global useing_proxy
    global proxy_history    
    if ip in useing_proxy:useing_proxy.remove(ip)
    if ip in proxy_history:proxy_history[ip][1]+=1
def goodip(ip):
    global init_time
    if time.clock() - init_time > int(config.get('proxy','proxy_fresh_time'))/100:
        proxy_init()  
    global available_proxy
    global useing_proxy
    if ip in useing_proxy:useing_proxy.remove(ip)
    if ip not in available_proxy: available_proxy.append(ip)
    if ip in proxy_history:proxy_history[ip][0]+=1
global init_time
init_time = time.clock()
if not os.path.exists(config.get('file','proxyfile')):proxy_init()
proxy_getlist()
print('proxy init')
#proxy_getlist()
